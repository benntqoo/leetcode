package easy

object TwoSum {
    fun main(nums: IntArray, target: Int): IntArray {
        HashMap<Int, Int>().apply {
            nums.forEachIndexed { index, i ->
                val targetKey = target - i
                if (containsKey(targetKey)) return intArrayOf(get(targetKey) ?: 0, index)
                put(i, index)
            }
        }
        return intArrayOf()
    }
}