package medium

object Divide {
    fun main(dividend: Int, divisor: Int): Int {
        if (dividend == divisor) return 1       //两数相同
        if (divisor == 1) return dividend       //除数为1
        if (dividend == 0) return 0             //被除数为0
        if (divisor == Int.MAX_VALUE) return 0  //除数为 int 最大值

        return 0
    }
}