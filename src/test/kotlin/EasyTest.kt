import easy.*
import kotlin.test.*

class EasyTest {
    @Test
    fun twoSum(){
        // 示例一
        println("示例一")
        val firstNumbers = intArrayOf(2, 7, 11, 15)
        val firstTarget = 9
        println(TwoSum.main(firstNumbers, firstTarget).contentToString())

        print("\n")

        // 示例二
        println("示例二")
        val secondNumbers = intArrayOf(3, 2, 4)
        val secondTarget = 6
        println(TwoSum.main(secondNumbers, secondTarget).contentToString())

        print("\n")

        // 示例三
        println("示例三")
        val thirdNumbers = intArrayOf(3, 3)
        val thirdTarget = 6
        println(TwoSum.main(thirdNumbers, thirdTarget).contentToString())
    }
}